﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoneBookApp
{
    class Contact
    {

        public string FullName { get; set; }
        public string Gender { get; set; }
        public int Code { get; set; }
        public int Number { get; set; }
        public string FullNumber { get; set; }
        public string Country { get; set; }
        public string Regions { get; set; }
        private string operantor;

        public string Operator
        {
            get
            {
                if (Code == 93 || Code == 77 || Code == 94 || Code == 98)
                    return "Vivacell";
                else if (Code == 43 || Code == 91 || Code == 96 || Code == 99)
                    return "Beeline";
                else
                    return "Orange";
            }
            set { operantor = value; }
        }
    }
}
