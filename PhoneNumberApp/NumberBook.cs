﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoneBookApp
{
    class NumberBook 
    {
        public Dictionary<int, Contact> PhoneBook = new Dictionary<int, Contact>();
        private int[] cods = { 43, 91, 96, 99, 93, 77, 94, 98, 55, 95, 41 };
        private string[] gender = { "Female", "Male" };
        private string[] regions = { "Aragatsotn", "Ararat", "Armavir", "Gegharkunik", "Kotayk","Lori","Shirak","Syunik","Tavush","Vayots Dzor", "Yerevan" };
        public Dictionary<int, Contact> CreatePhoneBook(int count)
        {
            var dic = new Dictionary<int, Contact>();
            Random rand = new Random();
            for (int i = 0; i < count; i++)
            {
                Contact c = new Contact();
                c.FullName = $"Anun{rand.Next(1, 50)}-yan";
                c.Country = "Armenia";
                c.Regions = regions[rand.Next(0, regions.Length)];
                c.Gender = gender[rand.Next(0, gender.Length)];
                c.Code = cods[rand.Next(0, cods.Length)];
                c.Number = rand.Next(100000, 1000000);
                c.FullNumber = $"(+374) {c.Code}-{string.Format("{0:##-##-##}", c.Number)}";
                while(dic.ContainsKey(c.Number))
                {
                c.Number = rand.Next(100000, 1000000);
                }
                dic.Add(c.Number, c);
            }
            return dic;
        }
        public void Print(Dictionary<int,Contact> dic)
        {
            foreach (var item in dic)
            {
                PrintContact(item.Value);
            }
        }
        public void PrintContact(Contact con)
        {
            Console.ForegroundColor = ConsoleColor.DarkCyan;
            Console.WriteLine(con.FullNumber + " (" + con.Operator+ ")\t " + con.FullName + "\t" + con.Gender +"\t\t" + con.Country + ",  " + con.Regions);
        }
    }
}
