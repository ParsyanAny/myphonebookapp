using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoneBookApp
{
    class Program
    {

        static void Main(string[] args)
        {
            string a = null;
            NumberBook book = new NumberBook();
            var phonebook = book.CreatePhoneBook(50);
            while (a != "exit")
            {
                MyPhoneBookApp(book, phonebook);

                PaintExitHelper();
                a = Console.ReadLine();
                Console.Clear();
            }
            Console.Read();

        }
        static public void MyPhoneBookApp(NumberBook book, Dictionary<int,Contact> phonebook)
        {
            PaintStartPage();
            string choose;
            while (true)
            #region 
            {
                choose = Console.ReadLine();
                if (choose == "1" || choose == "2" || choose == "3" || choose == "4" || choose == "5")
                    break;
                else
                {
                    PaintTryAgain();
                }
            }
            #endregion
            switch (choose)
            {
                case "1":
                    Console.Clear();
                    book.Print(phonebook);
                    Console.ReadKey();
                    Console.Clear();
                    break;
                case "2":
                    #region
                    PaintEnterString("Number");
                    while (true)
                    {
                        int number = Int32.Parse(Console.ReadLine());
                        Console.WriteLine("\n\n");
                        if (phonebook.ContainsKey(number))
                        {
                            book.PrintContact(phonebook[number]);
                            break;
                        }
                        PaintNumberNotFoundAndHelp(phonebook);
                        
                    }
                    Console.ReadKey();
                    Console.Clear();
                    break;
                #endregion
                case "3":
                    #region
                    PaintEnterString("Name");
                        int m = 0;
                        int name = Int32.Parse(Console.ReadLine());
                        string yourName = $"Anun{name}-yan";
                        Console.WriteLine("\n\n");
                        foreach (var item in phonebook.Values)
                        {
                            if (item.FullName == yourName)
                            {
                                book.PrintContact(item);
                                m++;
                            }
                        }
                        if (m == 0)
                            PaintNotFound();
                    Console.ReadKey();
                    Console.Clear();
                    break;
                #endregion
                case "4":
                    #region
                    PaintEnterString("Gender");
                    string gender = Console.ReadLine();
                    string gen = null;
                    Console.WriteLine();
                    switch (gender)
                    {
                        case "1":
                            gen = "Male";
                            break;
                        case "2":
                            gen = "Female";
                            break;
                        default:
                            PaintTryAgain();
                            break; 
                    }
                    int g = 0;
                    foreach (var item in phonebook.Values)
                    {
                        if (item.Gender == gen)
                        {
                            book.PrintContact(item);
                            g++;
                        }
                    }
                    if (g == 0)
                        Console.WriteLine("Not Found");
                    Console.ReadKey();
                    Console.Clear();
                    break;
                #endregion
                case "5":
                    #region
                    PaintEnterString("Code");
                    int num = Int32.Parse(Console.ReadLine());
                    int n = 0;
                    Console.WriteLine("\n\n\n");
                    foreach (var item in phonebook.Values)
                    {
                        if (item.Code == num)
                        {
                            book.PrintContact(item);
                            n++;
                        }
                    }
                    if (n == 0)
                        PaintNotFound();
                    Console.ReadKey();
                    Console.Clear();
                    break;
                #endregion
                case "6":
                   
                        break;
            }
        }
        static public void PaintStartPage()
        {
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.SetCursorPosition(55, 3);
            Console.WriteLine("Hello, user!");
            Console.SetCursorPosition(40, 6);
            Console.WriteLine("1) If You Want To Open Phonebook, Please Enter '1'");
            Console.SetCursorPosition(40, 7);
            Console.WriteLine("2) For Search By Number Please Enter '2'");
            Console.SetCursorPosition(40, 8);
            Console.WriteLine("3) For Search By Name Please Enter '3'");
            Console.SetCursorPosition(40, 9);
            Console.WriteLine("4) For Search By Gender Please Enter '4'");
            Console.SetCursorPosition(40, 10);
            Console.WriteLine("5) For Search By Code Please Enter '5'");
            Console.ForegroundColor = ConsoleColor.White;
            Console.SetCursorPosition(57, 12);
            Console.WriteLine("________");
            Console.SetCursorPosition(1, 13);
            Console.WriteLine(new string('_',118));
            Console.SetCursorPosition(60, 12);
        }
        static public void PaintTryAgain()
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.SetCursorPosition(60, 16);
            Console.WriteLine("           ");
            Console.SetCursorPosition(54, 15);
            Console.WriteLine("  Try Again!");
            Console.SetCursorPosition(57, 12);
            Console.WriteLine("________\t\t\t\t\t");
            Console.ResetColor();
            Console.SetCursorPosition(60, 12);
        }
        static public void PaintEnterString(string st)
        {
            Console.Clear();
            Console.SetCursorPosition(1, 13);
            Console.WriteLine(new string('_', 118));
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.SetCursorPosition(52,10);
            Console.WriteLine("___________");
            Console.SetCursorPosition(50,8);
            Console.WriteLine($"Enter Your {st}");
            Console.SetCursorPosition(56,10);
            if (st == "Gender")
            {
                Console.WriteLine();
                Console.SetCursorPosition(48,10);
                Console.WriteLine("1)For Male Enter '1'");
                Console.SetCursorPosition(48, 11);
                Console.WriteLine("2)For Female Enter '2'");
                Console.SetCursorPosition(58, 12);
                Console.WriteLine("- -");
                Console.SetCursorPosition(59,12);
            }
        }
        static public void PaintNotFound()
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.SetCursorPosition(53, 15);
            Console.WriteLine("Not Found");
            Console.ResetColor();
            
        }
        static public void PaintNumberNotFoundAndHelp(Dictionary<int,Contact> dic)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.SetCursorPosition(1, 1);
            Console.WriteLine("Number Not Found");
            Console.ForegroundColor = ConsoleColor.Cyan;
            foreach (var item in dic)
            {
                Console.Write(item.Key + "  ");
            }
            Console.SetCursorPosition(56, 10);
            Console.WriteLine("________");
            Console.SetCursorPosition(56, 10);
        }
        public static void PaintExitHelper()
        {
            Console.ForegroundColor = ConsoleColor.DarkCyan;
            Console.SetCursorPosition(45, 10);
            Console.WriteLine("Press 'ENTER' to Continue");
            Console.SetCursorPosition(45, 11);
            Console.WriteLine("For Exit enter 'exit'");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.SetCursorPosition(50, 13);
        }
    }
}
